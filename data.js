const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}

//Problem1 - Find all users who are interested in video games.

let videoGamePlayers = Object.entries(users)
.filter((object) => {
    object[1].interests = object[1].interests.join(" ")
    return object[1].interests.includes("Video Games")
})
.map((object) => {
    return object[0]
})
// console.log(videoamePlayers)


//Problem2 - Find all users staying in Germany.

let germanyStayers = Object.entries(users)
.filter((object) => {
    return object[1].nationality.includes("Germany")
})
.map((object) => {
    return object[0]
})
// console.log(germanyStayers)


// PROBLEM3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
let sortByPosition = Object.entries(users)
.sort((obj1, obj2) => {
    let obj1Designation = obj1[1].desgination
    let obj2Designation = obj2[1].desgination
    if (obj1Designation.includes("Senoior") || obj2Designation.includes("Senior")){
        return 1
    } else if (obj1Designation.includes("Intern") || obj2Designation.includes("Intern")){
        return -1
    } else{
        return 1
    }
})
console.log(sortByPosition)

let sortByAge = Object.fromEntries(Object.entries(users)
.sort((array1, array2) => {
    return array2[1].age - array1[1].age
}))
// console.log(sortByAge)

//Problem4 - Find all users with masters Degree.

let mastersHolders = Object.entries(users)
.filter((object) => {
    return object[1].qualification.includes("Masters")
})
.map((object) => {
    return object[0]
})
console.log(mastersHolders)
